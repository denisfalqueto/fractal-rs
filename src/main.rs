use fractal::Mandelbrot;

mod fractal;
mod ui;

fn main() {
    let mand = Mandelbrot::new(500);

    let point = (-0.748, 0.1);
    let radius = 0.0014;

    let cores = vec![
        image::Rgb([0, 0, 255]),
        image::Rgb([255, 255, 0]),
        image::Rgb([255, 255, 255]),
    ];

    let img = ui::gera_fractal(&mand, 2000, 1080, &point, radius, &cores);
    img.save("target/teste.png").unwrap();
}
