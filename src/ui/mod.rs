use super::fractal::Fractal;
use image::{ImageBuffer, RgbImage};

pub fn gera_fractal<T>(
    fractal: &T,
    width: u32,
    height: u32,
    point: &(f64, f64),
    radius: f64,
    colors: &Vec<image::Rgb<u8>>,
) -> RgbImage
where
    T: Fractal + Sync,
{
    let mut result: RgbImage = ImageBuffer::new(width, height);

    let parte = 100 as f64 / (colors.len() - 1) as f64;

    let from = (point.0 - radius, point.1 - radius);
    let to = (point.0 + radius, point.1 + radius);

    let diff = (to.0 - from.0, to.1 - from.1);
    let inc_complex = (diff.0 / width as f64, diff.1 / height as f64);

    rayon::scope(|s| {
        for (_, row) in result.enumerate_rows_mut() {
            s.spawn(move |_| {
                for (x, y, px) in row {
                    let c = (
                        from.0 + x as f64 * &inc_complex.0,
                        from.1 + y as f64 * &inc_complex.1,
                    );

                    match fractal.calculate(&c) {
                        Some(i) => {
                            let mut porcentagem = i as f64 / fractal.num_iteracoes() as f64;

                            let mut cor_idx = 0;
                            while porcentagem > parte {
                                porcentagem -= parte;
                                cor_idx += 1;
                            }

                            // Reajustar a porcentagem em relação a parte final
                            porcentagem = porcentagem * 100 as f64 / parte;

                            let from_color = colors.get(cor_idx).unwrap();
                            let to_color = colors.get(cor_idx + 1).unwrap();

                            let mut rgb: [u8; 3] = [0, 0, 0];
                            for i in 0..3 {
                                rgb[i] = (from_color.0[i] as f64
                                    + (porcentagem * (to_color.0[i] as f64 - from_color.0[i] as f64)))
                                    as u8;
                            }
                            *px = image::Rgb(rgb);
                        }
                        None => {
                            *px = image::Rgb([0, 0, 0]);
                        }
                    }
                }
            });
        }
    });

    result
}
