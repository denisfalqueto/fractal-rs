pub trait Fractal {
    fn min_value() -> (f64, f64);

    fn max_value() -> (f64, f64);

    fn num_iteracoes(&self) -> u32;

    fn calculate(&self, c: &(f64, f64)) -> Option<u32>;
}

pub struct Mandelbrot {
    iterations: u32,
}

impl Mandelbrot {
    pub fn new(iterations: u32) -> Mandelbrot {
        Mandelbrot {
            iterations: iterations,
        }
    }
}

impl Fractal for Mandelbrot {
    fn min_value() -> (f64, f64) {
        (-2.5, -1.0)
    }

    fn max_value() -> (f64, f64) {
        (2.0, 1.0)
    }

    fn num_iteracoes(&self) -> u32 {
        self.iterations
    }

    fn calculate(&self, c: &(f64, f64)) -> Option<u32> {
        let mut x = 0.0;
        let mut y = 0.0;
        for idx in 0..self.iterations {
            let tmp = &x * &x - &y * &y + c.0;
            y = 2.0 * &x * &y + c.1;
            x = tmp;
            if &x * &x + &y * &y > 4.0 {
                return Some(idx);
            }
        }
        None
    }
}
